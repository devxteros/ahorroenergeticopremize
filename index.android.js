/**
 * Index
 * Juan Manuel Lora <juanm.lora@premize.com>
 * 2017-09-25
 */

import { AppRegistry } from 'react-native';
import { app } from './src/app';

AppRegistry.registerComponent('AhorroEnergetico', () => app);
