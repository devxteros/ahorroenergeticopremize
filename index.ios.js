/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry
  } from 'react-native';

import app from './src/app';
AppRegistry.registerComponent('AhorroEnergetico', () => app);