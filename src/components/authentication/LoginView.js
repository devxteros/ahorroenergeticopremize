/**
 * Vista Login (Componente Autenticación)
 * Juan Manuel Lora <juanm.lora@premize.com>
 * 2017-09-25
 */

import React, { Component } from 'react';
import {Text, View, TextInput, Button, Image, ScrollView, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationActions } from 'react-navigation';
import {styles} from './styles';
import { COLOR } from '../../config/colors';
import { MESSAGES } from './config/messages';
import {actionLogin} from './actions/auth';

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Main'})
  ]
});

export default class LoginView extends Component {

  state= {
    username:'',
    password: '',
    isLogin: false
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.logo}>
          <Image source={require('./assets/logo.png')} style={{width: 150, height: 150}} resizeMode="contain"/>
        </View>

        <View style={{flex:1}}>
            <View style={styles.inputTextLayer}>
              <View style={styles.inputIcon}>
                <Icon name="account" size={30} color={COLOR.PRIMARY_DARK} />
              </View>
              <View style={{flex:1}}>
                <TextInput
                  ref="1"
                  returnKeyType='next'
                  onSubmitEditing={() => this.focusNextField('2')}
                  keyboardType='email-address'
                  style={styles.inputText}
                  onChangeText={(username) => this.setState({username})}
                  value={this.state.username}
                  underlineColorAndroid={'transparent'}
                  placeholder={MESSAGES.USERNAME}
                  placeholderTextColor={COLOR.PRIMARY_DARK}
                  maxLength={50}
                />
              </View>
            </View>

            <View style={styles.inputTextLayer}>
              <View style={styles.inputIcon}>
                <Icon name="lock" size={30} color={COLOR.PRIMARY_DARK} />
              </View>
              <View style={{flex:1}}>
              <TextInput
                ref="2"
                keyboardType='default'
                style={styles.inputText}
                onChangeText={(password) => this.setState({password})}
                value={this.state.password}
                underlineColorAndroid={'transparent'}
                secureTextEntry={true}
                placeholder={MESSAGES.PASSWORD}
                placeholderTextColor={COLOR.PRIMARY_DARK}
                maxLength={50}
              />
              </View>
            </View>
        </View>

        <View style={{flex:1, paddingTop:60}}>
          <Button
            onPress={this.onLogin.bind(this)}
            title={(this.state.isLogin) ? MESSAGES.ONLOGIN : MESSAGES.LOGIN}
            color={COLOR.PRIMARY}            
          />
        </View>
      </ScrollView>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onLogin(){
    if(this.state.username == '' || this.state.password == ''){
      ToastAndroid.show(MESSAGES.EMPTY_FIELDS, ToastAndroid.SHORT);
    }else{
      this.setState({isLogin:true});
      actionLogin(this.state.username, this.state.password)
      .then((response) => response.json())
      .then((responseData)=>{
        if(responseData.result){
          //alert(responseData.data[0].name);
          this.props.navigation.dispatch(resetAction)
        }else{
          ToastAndroid.show(MESSAGES.INVALID_AUTH, ToastAndroid.SHORT);
          this.setState({isLogin:false});
        }
      });
    }
    
  }

}