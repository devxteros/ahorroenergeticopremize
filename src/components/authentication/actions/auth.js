import { CONFIG } from '../config/config';

export const actionLogin = (username:string, password:string) => {
  
  	return fetch(CONFIG.API_URL, {
	  method: 'POST',
	  headers: {
	    'Accept': 'application/json',
	    'Content-Type': 'application/json',
	  },
	  body: JSON.stringify({
	    username: username,
	    password: password,
	  })
	});
};