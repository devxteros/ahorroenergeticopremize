import React from 'react';
import { StyleSheet } from 'react-native';
import { COLOR } from '../../config/colors';

export const styles = StyleSheet.create({
  
  /* Container */
  container: {
    flex: 1,    
    padding:20
  },
  
  /* Logo */
  logo:{
    flex:1,
    alignItems:'center'
  },

  /* Input */
  inputTextLayer:{    
    flexDirection:'row',
  },
  inputText:{    
    borderLeftWidth:0,
    borderRightWidth:0,
    borderTopWidth:0,
    borderBottomWidth:1,
    borderBottomColor:COLOR.PRIMARY_DARK,  
    color:COLOR.PRIMARY_DARK  
  },
  inputIcon:{
    borderBottomWidth:1,
    borderBottomColor:COLOR.PRIMARY_DARK,
    alignItems:'center',
    justifyContent:'center'
  },
  
});