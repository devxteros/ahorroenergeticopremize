import React, { Component } from 'react';
import {Text, View } from 'react-native';
import {styles} from './styles';

export default class ForgotPasswordView extends Component {

  static navigationOptions = {
    title: 'Recordar contraseña',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          ForgotPassword
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.android.js
        </Text>
        <Text style={styles.instructions}>
          Double tap R on your keyboard to reload,{'\n'}
          Shake or press menu button for dev menu
        </Text>
      </View>
    );
  }
}