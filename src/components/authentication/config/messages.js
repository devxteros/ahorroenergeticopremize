export const MESSAGES = {	
	USERNAME: 'Usuario',
	PASSWORD: 'Contraseña',
	LOGIN: 'Ingresar',
	ONLOGIN: 'Ingresando...',
	EMPTY_FIELDS: 'Usuario y Contraseña son obligatórios.',
	INVALID_AUTH: 'Usuario / Contraseña incorrectos.'
}