import React, { Component } from 'react';
import {Text, View } from 'react-native';
import {styles} from './styles';

export default class RegisterView extends Component {

    static navigationOptions = {
      title: 'Registrar',
    };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Register
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.android.js
        </Text>
        <Text style={styles.instructions}>
          Double tap R on your keyboard to reload,{'\n'}
          Shake or press menu button for dev menu
        </Text>
      </View>
    );
  }
}