/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ItemBox extends Component {


  state = {
    estado:'',
  };

  item = {tipo, nombre, serial, estado} = this.props.artist;

  render() {    

    return (      
      <View style={styles.taskbox}>
        <View style={styles.iconContainer}>
          <Image style={{flex: 1, width:100}} resizeMode={'cover'} source={{uri: (imagen=='')?'http://via.placeholder.com/100x100':imagen}} />
        </View>
        <View style={styles.info}>
            <View>
              <Text style={styles.task}>{nombre}</Text>
            </View>
            <View>
              <Text style={styles.customer}>{tipo}</Text>
            </View>
            <View>              
              <Text style={styles.date}>{serial}</Text>
            </View>
            <View>
              <Text style={styles.project}>{estado}</Text>
            </View>          
        </View>
        <View style={{flex:1}}>
          <Text>{this.state.estados}</Text>
          <Switch
          onValueChange={(value) => this.setState({estado: value})}
          style={{marginBottom: 10}}
          value={this.state.estado} />
        </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  taskbox: {
    margin: 1,
    backgroundColor: 'white',
    flexDirection: 'row',    
  },
  info: {
    flex: 4,
    alignItems: 'stretch',
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 5
  },
  date: {
    fontSize: 12,    
    color: '#999',
  },
  customer: {
    fontSize: 12,    
    color: '#999',
  },
  task: {
    fontSize: 15,    
    color: '#222',
    fontWeight: '400',
  },
  project: {
    fontSize: 13,    
    color: '#777',
  },
  iconContainer: {    
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#ddd'
  },
  hours: {
    fontSize: 15,    
    color: '#777',
    fontWeight: 'bold',    
  },  
});