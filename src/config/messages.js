export const MESSAGES = {
	NAMEAPP: 'Ahorro Energético',
	EVENTS: 'Eventos',
	DEVICES: 'Dispositivos',
	SETTINGS: 'Configuración',
	USERNAME: 'Usuario',
	PASSWORD: 'Contraseña',
	LOGIN: 'Ingresar',
	ONLOGIN: 'Ingresando...',
	EMPTY_EVENTS: 'No tiene reservas',
	EMPTY_DEVICES: 'No tiene dispositivos',
}