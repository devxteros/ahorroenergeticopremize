/* 
Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences.
COLOR TOOL
Create, share, and apply color palettes to your UI, as well as measure the accessibility level of any color combination.
Visit: https://material.io/color
*/
export const COLOR = {
	PRIMARY:'#689f38',
	PRIMARY_LIGHT:'#99d066',
	PRIMARY_DARK:'#387002',
	SECONDARY:'#0288d1',
	SECONDARY_LIGHT:'#5eb8ff',
	SECONDARY_DARK:'#005b9f',
	TEXT_PRIMARY:'#ffffff',
	TEXT_SECONDARY:'#ffffff',
	BLACK:'#000000',
	WHITE:'#ffffff'
}