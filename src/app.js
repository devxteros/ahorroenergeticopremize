/**
 * Router
 * Juan Manuel Lora <juanm.lora@premize.com>
 * 2017-09-25
 */
import { StackNavigator } from 'react-navigation';
import MainView from './MainView';
import DevicesView from './DevicesView';
import LoginView from './components/authentication/LoginView';
import { headerToolbar } from './Toolbar';
import { COLOR } from './config/colors';

export const app = StackNavigator({
  Main: { screen: MainView, navigationOptions:({ navigation }) => {  
    return {
      //title: 'Home Title', // titulo de la aplicacion
      header: headerToolbar(navigation), //Componente de la barra de navegación
    }
  }},
  Devices: { screen: DevicesView },
  Login: { screen: LoginView, navigationOptions:{header:null}},
},
{
  navigationOptions:{
    headerTintColor: COLOR.TEXT_PRIMARY, //Color del texto en la barra de titulo
    headerStyle: {backgroundColor: COLOR.PRIMARY},
  },
  initialRouteName:'Main',
  headerMode: 'screen'
}
);