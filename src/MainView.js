/**
 * Vista Inicial
 * Juan Manuel Lora <juanm.lora@premize.com>
 * 2017-09-25
 */

import React, { Component } from 'react';
import {View, Text, Button, Image, ToastAndroid } from 'react-native';
import ItemList from './components/ItemList';
import { styles } from './styles';
import { MESSAGES } from './config/messages';
import { COLOR } from './config/colors';
import { getReservas } from './actions/services';


export default class MainView extends Component {

	static navigationOptions = {
		title: MESSAGES.HOME,
	};

  state = {
    emptyData:true,
    items:[]
  };

componentWillMount(){
  
  try{
      
      getReservas()
      .then((response) => response.json())
      .then((jsonData) => {    
          if(jsonData.length>0){

          let listaEventos=[];

              jsonData.forEach((evento)=>{
                let eventoObj = {
                'nombre':'',
                'ubicacion':'',
                'fecha_inicio':'',
                'fecha_fin':'',
                'estado':'EN CURSO',
                'imagen':'',
                'dispositivos':[]
              };               

                eventoObj.nombre = evento.nombre;
                eventoObj.ubicacion=evento.espacio.nombre;
                eventoObj.fecha_inicio=evento.fechaInicio;
                eventoObj.fecha_fin=evento.fechaFin;

                if(evento.espacio.dispositivosDto.length > 0){
                    let listaDispositivos=[];
                    evento.espacio.dispositivosDto.forEach((dispositivo)=>{
                        let dispositivoObj = {
                                'tipo':'',
                                'nombre':'',
                                'serial':'',
                                'estado':''
                            };
                      dispositivoObj.tipo = dispositivo.tipoDispositivo.nombre;
                      dispositivoObj.nombre = dispositivo.nombre;
                      dispositivoObj.serial = dispositivo.serial;
                      dispositivoObj.estado = '';
                      listaDispositivos.push(dispositivoObj);
                    });

                    eventoObj.dispositivos = listaDispositivos;
                }

                listaEventos.push(eventoObj);
              });

              this.setState({items:listaEventos});
              this.setState({emptyData:false});
            
          }
      }).catch((error) => {
          alert(error);        
      });

  }catch(e){
    alert(e);    
  }
  


}


render() {

    	const { navigate } = this.props.navigation;

      return (
        <View style={{flex:1}}>        
          {this._renderEmptyData()}
          <View style={{flex:10, marginTop:10}}>        
          {this._renderViewItems()}
          </View>
        </View>
      );
}

_renderViewItems(){
    if(!this.state.emptyData){
      return(
        <ItemList items={this.state.items} navigator={this.props.navigation} />
      )
    }else{
      return null;
    }
}

_renderEmptyData(){
    if(this.state.emptyData){
      return(
        <View style={styles.emptydata}>
          <Image source={require('./assets/calendar.png')} style={{width: 80, height: 80}} resizeMode="contain"/>   
          <Text style={styles.labelText}>{MESSAGES.EMPTY_EVENTS}</Text>
        </View>
      )
    }else{
      return null;
    }
}


  }

