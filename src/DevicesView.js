/**
 * Vista de Eventos
 * Juan Manuel Lora <juanm.lora@premize.com>
 * 2017-09-25
 */

import React, { Component } from 'react';
import {View, Text, Button, Image} from 'react-native';
import ItemListDevice from './components/ItemListDevice';
import { styles } from './styles';
import { MESSAGES } from './config/messages';
import { COLOR } from './config/colors';


export default class DevicesView extends Component {

	static navigationOptions = {
		title: MESSAGES.DEVICES,
	};

  state = {
    emptyData:true,
    devices:[]
  };


  componentWillMount(){
    let { params } = this.props.navigation.state;    
    if(params.item.dispositivos.length>0){
      this.setState({devices:params.item.dispositivos});
      this.setState({emptyData:false});
    }
  }

  render() {  	
    return (      
      <View style={{flex:1}}>
          {this._renderEmptyData()}
          <View style={{flex:10, marginTop:10}}>        
          {this._renderViewItems()}
          </View>
        </View>
    );
  }

  _renderViewItems(){
    if(!this.state.emptyData){
      return(
        <ItemListDevice items={this.state.devices} navigator={this.props.navigation} />
      )
    }else{
      return null;
    }
  }

  _renderEmptyData(){
    if(this.state.emptyData){
      return(
        <View style={styles.emptydata}>
          <Image source={require('./assets/calendar.png')} style={{width: 80, height: 80}} resizeMode="contain"/>   
          <Text style={styles.labelText}>{MESSAGES.EMPTY_DEVICES}</Text>
        </View>
      )
    }else{
      return null;
    }
  }
}