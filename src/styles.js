import { StyleSheet } from 'react-native';
import { COLOR } from './config/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  emptydata:{
    flex:1,
    alignItems:'center',
    paddingTop:100
  },
  labelText: {
    fontSize: 17,
    color: '#333333',
    letterSpacing: 0.5
  },
});